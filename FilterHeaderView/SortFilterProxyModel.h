﻿//////////////////////////////////////////////////////////////////////////
/// Copyright (C), 1998-2016, rytec Corporation. All rights reserved.
/// \file       SortFilterProxyModel.h
/// \brief      
/// \author     wyf
/// \version    0.95
/// \date       2020-9
//////////////////////////////////////////////////////////////////////////
#ifndef _FILTERHEADERVIEW_SORTFILTERPROXYMODEL_H_
#define _FILTERHEADERVIEW_SORTFILTERPROXYMODEL_H_

#include <QSortFilterProxyModel>

/// \brief 排序过滤代理模型类
class CRySortFilterProxyModel : public QSortFilterProxyModel
{
public:
    /// \brief 构造函数
    /// \param [in] parent 父对象
    CRySortFilterProxyModel(QObject* parent = NULL);

public:
    /// 参考 QSortFilterProxyModel::setHeaderData(int section, Qt::Orientation orientation,
    /// const QVariant& value, int role = Qt::EditRole)
    virtual bool setHeaderData(int section, Qt::Orientation orientation, const QVariant& value,
        int role = Qt::EditRole);

protected:
    /// 参考 QSortFilterProxyModel::filterAcceptsRow(int sourceRow,
    /// const QModelIndex& sourceParent) const
    virtual bool filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const;

private:
    /// \brief 判断过滤正则表达式是否为空
    /// \return 过滤正则表达式是否为空
    bool isFilterRegExpsEmpty() const;

private:
    /// 过滤表达式
    QMap<int, QRegExp> mFilterRegExps;
};

#endif // _FILTERHEADERVIEW_SORTFILTERPROXYMODEL_H_
