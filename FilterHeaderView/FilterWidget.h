﻿//////////////////////////////////////////////////////////////////////////
/// Copyright (C), 1998-2016, rytec Corporation. All rights reserved.
/// \file       ShowFilterWidget.h
/// \brief      
/// \author     wyf
/// \version    0.95
/// \date       2020-9
//////////////////////////////////////////////////////////////////////////

#ifndef _FILTERHEADERVIEW_SHOWFILTERWIDGET_H_
#define _FILTERHEADERVIEW_SHOWFILTERWIDGET_H_

#include <QWidget>
#include <QMap>

class QStandardItemModel;
class QListView;

/// \brief 过滤选项控件
class CRyFilterWidget : public QWidget
{
    Q_OBJECT
public:
    /// \brief 构造函数
    /// \param [in] parent 父控件
    explicit CRyFilterWidget(QWidget* parent = NULL);

public:
    /// \brief 获取表头索引
    /// \return 表头索引
    int getSection() const { return mSection; }

    /// \brief 设置表头索引
    /// \param [in] section 表头索引
    void setSection(int section) { mSection = section; }

    /// \brief 获取过滤数据
    /// \return 过滤数据
    void getFilterData(QStringList& texts, QList<bool>& checkStates) const;

    /// \brief 设置过滤数据
    /// \param [in] data 过滤数据
    void setFilterData(const QStringList& texts, const QList<bool>& checkStates);

Q_SIGNALS:
    /// \brief 过滤结束事件
    void filterFinished();

private Q_SLOTS:
    /// \brief 响应列表控件元素点击事件
    void onItemCliked(const QModelIndex& index);

protected:
    /// 参考 QWidget::hideEvent(QHideEvent* event)
    virtual void hideEvent(QHideEvent* event);

private:
    /// 表头索引
    int mSection;
    /// 列表控件
    QListView* mListView;
};

#endif // _FILTERHEADERVIEW_SHOWFILTERWIDGET_H_
