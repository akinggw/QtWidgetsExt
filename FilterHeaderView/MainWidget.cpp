﻿//////////////////////////////////////////////////////////////////////////
/// Copyright (C), 1998-2016, rytec Corporation. All rights reserved.
/// \file       MainWidget.cpp
/// \brief      
/// \author     wyf
/// \version    0.95
/// \date       2020-9
//////////////////////////////////////////////////////////////////////////

#include "MainWidget.h"
#include "SortFilterProxyModel.h"
#include "FilterHeaderView.h"
#include "FilterWidget.h"

#include <QSortFilterProxyModel>
#include <QStandardItemModel>
#include <QTableView>
#include <QHBoxLayout>

#pragma execution_character_set("utf-8")

CRyMainWidget::CRyMainWidget(QWidget *parent)
    : QWidget(parent)
{
    QHBoxLayout* mainLayout = new QHBoxLayout(this);
    mainLayout->setMargin(0);
    setLayout(mainLayout);

    mTableView = new QTableView(this);
    mainLayout->addWidget(mTableView);

    QStringList labels;
    labels << "ID" << "Name" << "Score" << "Level";
    mItemModel = new QStandardItemModel(this);
    mItemModel->setHorizontalHeaderLabels(labels);

    mFilterModel = new CRySortFilterProxyModel(this);
    mFilterModel->setSourceModel(mItemModel);
    mTableView->setModel(mFilterModel);

    CRyFilterHeaderView* headerView = new CRyFilterHeaderView(this);
    mTableView->setHorizontalHeader(headerView);
    headerView->setFilterVisible(1, true);
    headerView->setSortIndicator(2, Qt::AscendingOrder);
    headerView->setFilterVisible(3, true);

    mTableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    mTableView->setSelectionMode(QAbstractItemView::SingleSelection);
    mTableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    mTableView->setSortingEnabled(true);
    mTableView->verticalHeader()->hide();

    appendRow(QStringList() << "1" << "张三" << "80" << "abc");
    appendRow(QStringList() << "2" << "李四" << "70" << "abcdef");
    appendRow(QStringList() << "3" << "王五" << "90" << "ghi");
    appendRow(QStringList() << "4" << "赵六" << "50" << "jkl");
    resize(600, 300);
}

CRyMainWidget::~CRyMainWidget()
{

}

void CRyMainWidget::appendRow(const QStringList& itemTexts)
{
    QList<QStandardItem*> items;
    for (const auto& text : itemTexts)
    {
        QStandardItem* item = new QStandardItem();
        item->setData(text, Qt::DisplayRole);
        item->setData(text, Qt::ToolTipRole);
        items.append(item);
    }
    mItemModel->appendRow(items);
}
