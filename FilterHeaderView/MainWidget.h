﻿//////////////////////////////////////////////////////////////////////////
/// Copyright (C), 1998-2016, rytec Corporation. All rights reserved.
/// \file       MainWidget.h
/// \brief      
/// \author     wyf
/// \version    0.95
/// \date       2020-9
//////////////////////////////////////////////////////////////////////////
#ifndef _FILTERHEADERVIEW_MAINWIDGET_H_
#define _FILTERHEADERVIEW_MAINWIDGET_H_

#include <QWidget>

class QSortFilterProxyModel;
class QStandardItemModel;
class QTableView;

/// \brief 主控件
class CRyMainWidget : public QWidget
{
    Q_OBJECT
public:
    /// \brief 构造函数
    /// \param [in] parent 父控件
    CRyMainWidget(QWidget* parent = NULL);

    /// \brief 析构含糊
    ~CRyMainWidget();

private:
    void appendRow(const QStringList& items);

private:
    /// 
    QTableView* mTableView = nullptr;
    /// 
    QStandardItemModel* mItemModel = nullptr;
    /// 
    QSortFilterProxyModel* mFilterModel = nullptr;
};

#endif // _FILTERHEADERVIEW_MAINWIDGET_H_
