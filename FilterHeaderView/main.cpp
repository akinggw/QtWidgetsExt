﻿//////////////////////////////////////////////////////////////////////////
/// Copyright (C), 1998-2016, rytec Corporation. All rights reserved.
/// \file       main.cpp
/// \brief      
/// \author     wyf
/// \version    0.95
/// \date       2020-9
//////////////////////////////////////////////////////////////////////////

#include "MainWidget.h"
#include <QApplication>
#include <QFile>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);


    CRyMainWidget w;
    w.show();

    return app.exec();
}
