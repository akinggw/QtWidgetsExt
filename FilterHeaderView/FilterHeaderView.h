﻿//////////////////////////////////////////////////////////////////////////
/// Copyright (C), 1998-2016, rytec Corporation. All rights reserved.
/// \file       FilterHeaderView.h
/// \brief      
/// \author     wyf
/// \version    0.95
/// \date       2020-9
//////////////////////////////////////////////////////////////////////////
#ifndef _FILTERHEADERVIEW_FILTERHEADERVIEW_H_
#define _FILTERHEADERVIEW_FILTERHEADERVIEW_H_

#include <QHeaderView>

class CRyFilterWidget;

#define FILTER_REGEXP_ROLE      Qt::UserRole + 1000

/// \brief 带过滤的表头视图
class CRyFilterHeaderView : public QHeaderView
{
    Q_OBJECT
public:
    /// \brief 构造函数
    /// \param [in] parent 父控件
    CRyFilterHeaderView(QWidget* parent = NULL);

    /// \brief 析构函数
    virtual ~CRyFilterHeaderView();

public:
    /// \brief 获取过滤按钮常规图片
    /// \return 过滤按钮常规图片
    QPixmap getFilterPixmap(int state) const;

    /// \brief 设置过滤按钮常规图片
    /// \param [in] pixmap 过滤按钮常规图片
    void setFilterPixmap(int state, const QPixmap& pixmap);

    /// \brief 获取过滤按钮是否可见
    /// \param [in] section 列索引
    /// \return 过滤按钮是否可见
    bool getFilterVisible(int section) const { return mFilterVisible.value(section); }

    /// \brief 设置过滤按钮是否可见
    /// \param [in] section 列索引
    /// \param [in] visible 过滤按钮是否可见
    void setFilterVisible(int section, bool visible) { mFilterVisible.insert(section, visible); }

public:
    /// 参考 QHeaderView::setModel(QAbstractItemModel* model);
    virtual void setModel(QAbstractItemModel* model);

protected:
    /// 参考 QHeaderView::leaveEvent(QEvent* event)
    virtual void leaveEvent(QEvent* event);
    /// 参考 QHeaderView::mouseMoveEvent(QMouseEvent* event)
    virtual void mouseMoveEvent(QMouseEvent* event);
    /// 参考 QHeaderView::mousePressEvent(QMouseEvent* event)
    virtual void mousePressEvent(QMouseEvent* event);
    /// 参考 QHeaderView::mouseReleaseEvent(QMouseEvent* event)
    virtual void mouseReleaseEvent(QMouseEvent* event);
    /// 参考 QHeaderView::paintSection(QPainter* painter, const QRect& rect, int logicalIndex) const
    virtual void paintSection(QPainter* painter, const QRect& rect, int logicalIndex) const;

private:
    /// \brief 获取特定列及其子元素列的文本集合
    /// \param [in] index 索引
    /// \param [in, out] texts 文本集合
    void getItemTexts(QAbstractItemModel* itemModel, const QModelIndex& index, QStringList& texts);

private:
    /// \brief 响应过滤条件变化事件
    void onRowsChanged(const QModelIndex& parent, int first, int last);
    /// \brief 响应过滤条件变化事件
    void onColumnsChanged(const QModelIndex& parent, int first, int last);
    /// \brief 响应过滤条件变化事件
    void onDataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight, 
        const QVector<int>& roles = QVector<int>());
    /// \brief 响应过滤条件变化事件
    void onFilterFinished();

private:
    /// 过滤按钮状态: 0 常规; 1 悬停; 2 按下; 
    int mFilterState;
    /// 当前表索引头
    int mCurSection;
    /// 过滤按钮图标
    QPixmap mFilterPixmap[3];
    /// 过滤按钮是否可见
    QMap<int, bool> mFilterVisible;
    /// 过滤条件显示控件
    CRyFilterWidget* mFilterWidget;
    /// 过滤文本集合
    QMap<int, QStringList> mFilterTexts;
    /// 过滤状态集合
    QMap<int, QList<bool>> mFilterStates;
};

#endif // _FILTERHEADERVIEW_FILTERHEADERVIEW_H_
