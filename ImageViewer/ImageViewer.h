//////////////////////////////////////////////////////////////////////////
/// Copyright (C), 1998-2016, rytec Corporation. All rights reserved.
/// \file       QtImageViewer.h
/// \brief      
/// \author     wyf
/// \version    0.95
/// \date       2020-3
//////////////////////////////////////////////////////////////////////////
#ifndef _QTIMAGEVIEWER_QTIMAGEVIEWER_H_
#define _QTIMAGEVIEWER_QTIMAGEVIEWER_H_

#include <QWidget>
#include <QImage>

class Ui_ImageViewer;
class QTimer;

/// \brief 图像查看器
class CRyImageViewer : public QWidget
{
	Q_OBJECT
public:
	/// \brief 构造函数
	/// \param [in] parent 父窗体
	CRyImageViewer(QWidget* parent = NULL);
	
    /// \brief 析构函数
    ~CRyImageViewer();

protected:
    /// 参考 \ref QWidget::enterEvent(QEvent* event)
    virtual void enterEvent(QEvent* event);
    /// 参考 \ref QWidget::leaveEvent(QEvent* event)
    virtual void leaveEvent(QEvent* event);
    /// 参考 \ref QWidget::paintEvent(QPaintEvent* event)
    virtual void paintEvent(QPaintEvent* event);
    /// 参考 \ref QWidget::wheelEvent(QWheelEvent* event)
    virtual void wheelEvent(QWheelEvent* event);
    /// 参考 \ref QWidget::mousePressEvent(QMouseEvent* event)
    virtual void mousePressEvent(QMouseEvent* event);
    /// 参考 \ref QWidget::mouseMoveEvent(QMouseEvent* event)
    virtual void mouseMoveEvent(QMouseEvent* event);
    /// 参考 \ref QWidget::mouseReleaseEvent(QMouseEvent* event)
    virtual void mouseReleaseEvent(QMouseEvent* event);
    /// 参考 \ref QWidget::eventFilter(QObject* watched, QEvent* event)
    virtual bool eventFilter(QObject* watched, QEvent* event);

private Q_SLOTS:
    /// \brief 响应重置按钮点击事件
    void onResetButtonClicked();
    /// \brief 响应全屏按钮点击事件
    void onFullScreenButtonClicked();
    /// \brief 响应旋转按钮点击事件
    void onRotateButtonClicked();
    /// \brief 响应保持按钮点击事件
    void onSaveButtonClicked();
    /// \brief 缩放信息显示超时事件
    void onScaleTextTimerOut();

private:
    /// \brief 缩放图像
    /// \param [in] ratio 缩放系数
    void scaleImage(qreal ratio, QPoint* center = NULL);
    
private:
    /// UI对象
    Ui_ImageViewer* mUi;
    /// 缩放系数文本显示计时器
    QTimer* mScaleTextTimer;

    /// 图像对象
    QImage mImage;
    /// 图像偏移量
    QPoint mOffset;
    /// 图像旋转角度
    qreal mRotation;
    /// 图像缩放系数
    qreal mScaleRatio;

    /// 按下的鼠标按键
    int mPressedButton;
    /// 上次鼠标位置
    QPoint mPreMousePos;
    /// 是否显示缩放系数文本信息
    bool mScaleTextVisible;
};

#endif // _QTIMAGEVIEWER_QTIMAGEVIEWER_H_
