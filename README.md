# QtWidgetsExt

#### 介绍
1. 扩展QDateTimeEdit、QTimeEdit、QDateEdit控件；
2. 实现了半透明效果的图片浏览器；
3. 实现带过滤条件的表头控件；
4、实现了类Photoshop色带控件；

#### 软件架构
1. 扩展QDateTimeEdit、QTimeEdit、QDateEdit控件；
2. 实现了半透明效果的图片浏览器；
3. 实现带过滤条件的表头控件；
4、实现了类Photoshop色带控件；

#### 安装教程

1.  使用git下载代码；
2.  使用Visual studio 2015打开解决方案；
3.  编译运行。

#### 运行截图

1.  时间日期控件

<img src="https://images.gitee.com/uploads/images/2020/0828/163413_8151ce51_5676427.jpeg" alt="时间日期控件1" width="301" height="200" align="bottom" />

2.  图片浏览器

<img src="https://images.gitee.com/uploads/images/2020/0902/174034_0e915aa1_5676427.jpeg" alt="图片浏览器控件" width="385" height="333" align="bottom" />

3. 过滤条件表头

<img src="https://images.gitee.com/uploads/images/2020/0902/174104_d71664df_5676427.jpeg" alt="过滤条件表头控件" width="315" height="102" align="bottom" />

4. 色带控件
<img src="https://images.gitee.com/uploads/images/2020/0909/084741_841274f2_5676427.jpeg" alt="色带控件" width="315" height="102" align="bottom" />

5. 弹出信息框
<img src="https://images.gitee.com/uploads/images/2021/0415/134431_f172151e_5676427.png" alt="弹出信息框" width="310" height="254" align="bottom" />

#### 其他说明

鸣谢：

  1. https://blog.csdn.net/jain_yu/article/details/104062618
  2. https://github.com/zhugp125/QtDemo.git

