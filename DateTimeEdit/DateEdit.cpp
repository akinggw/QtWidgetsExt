//////////////////////////////////////////////////////////////////////////
/// Copyright (C), 1998-2016, rytec Corporation. All rights reserved.
/// \file       DateEdit.cpp
/// \brief      
/// \author     wyf
/// \version    0.95
/// \date       2020-8
//////////////////////////////////////////////////////////////////////////

#include "DateEdit.h"
#include "CalendarWidget.h"

#include <QHBoxLayout>
#include <QPushButton>

CRyDateEdit::CRyDateEdit(QWidget *parent)
    : QDateEdit(parent)
    , mPopupButton(NULL)
    , mCalendarWidget(NULL)
{
    mPopupButton = new QPushButton();
    mPopupButton->setFixedSize(QSize(15, 15));
    mPopupButton->setCursor(QCursor(Qt::PointingHandCursor));
    mPopupButton->setStyleSheet(
        "QPushButton:hover{ background-color:rgba(37, 155, 243, 0.41); }"
        "QPushButton:pressed{ background-color:rgba(37, 155, 243, 0.99); }"
        "QPushButton{ border-image:url(:/Resources/PopupButton.png); }");

    QHBoxLayout* layout = new QHBoxLayout();
    layout->addStretch();
    layout->addWidget(mPopupButton);
    layout->setContentsMargins(0, 0, 20, 0);
    setLayout(layout);

    mCalendarWidget = new CRyCalendarWidget(displayedSections(), this);
    mCalendarWidget->setDateTime(dateTime());

    connect(mPopupButton, &QAbstractButton::clicked, this, &CRyDateEdit::onPopupButtonClicked);
    connect(mCalendarWidget, &CRyCalendarWidget::dataTimeChanged, this, &CRyDateEdit::onDateTimeChanged);
}

CRyDateEdit::~CRyDateEdit()
{
    delete mCalendarWidget;
    mCalendarWidget = NULL;
}

void CRyDateEdit::onPopupButtonClicked()
{
    QPoint position(0, height());
    position = mapToGlobal(position);

    mCalendarWidget->setDateTime(dateTime());
    mCalendarWidget->move(position);
    mCalendarWidget->show();
}

void CRyDateEdit::onDateTimeChanged(const QDateTime& dt)
{
    setDateTime(dt);
}

void CRyDateEdit::setDisplayFormat(const QString &format)
{
    QDateTimeEdit::setDisplayFormat(format);
    mCalendarWidget->setDisplaySections(displayedSections());
}
