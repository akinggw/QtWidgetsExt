//////////////////////////////////////////////////////////////////////////
/// Copyright (C), 1998-2016, rytec Corporation. All rights reserved.
/// \file       DateEdit.h
/// \brief      
/// \author     wyf
/// \version    0.95
/// \date       2020-8
//////////////////////////////////////////////////////////////////////////

#ifndef _TESTDATEEDIT_DATEEDIT_H_
#define _TESTDATEEDIT_DATEEDIT_H_

#include <QDateEdit>
#include <QPushButton>
#include <QLabel>

class CRyDayLabel;
class CRyCalendarWidget;

/// \brief 日期控件
class CRyDateEdit : public QDateEdit
{
    Q_OBJECT
public:
    /// \brief 默认构造函数
    /// \param [in] parent 父对象
    explicit CRyDateEdit(QWidget* parent = NULL);

    /// \brief 析构函数
    ~CRyDateEdit();

public:
    /// \brief 设置日期显示格式
    /// \param [in] format 日期显示格式
    void setDisplayFormat(const QString& format);

protected Q_SLOTS:
    /// \brief 响应弹出日历控件按钮点击事件
    void onPopupButtonClicked();

    /// \brief 响应日历控件日期变化事件
    void onDateTimeChanged(const QDateTime& dateTime);

private:
    /// 弹出日历控件按钮
    QPushButton* mPopupButton;
    /// 日历控件
    CRyCalendarWidget* mCalendarWidget;
};

#endif // _TESTDATEEDIT_DATEEDIT_H_
