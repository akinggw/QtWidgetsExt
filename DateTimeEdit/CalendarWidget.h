//////////////////////////////////////////////////////////////////////////
/// Copyright (C), 1998-2016, rytec Corporation. All rights reserved.
/// \file       CalendarWidget.h
/// \brief      
/// \author     wyf
/// \version    0.95
/// \date       2020-8
//////////////////////////////////////////////////////////////////////////

#ifndef _TESTDATEEDIT_CALENDARWIDGET_H_
#define _TESTDATEEDIT_CALENDARWIDGET_H_

#include <QDateTimeEdit>
#include <QDateTime>
#include <QLabel>

class QHBoxLayout;
class QVBoxLayout;
class QGridLayout;
class QPushButton;
class QComboBox;

/// \brief 日期标签
class CRyDayLabel : public QLabel
{
    Q_OBJECT
public:
    /// \brief 日期类型
    enum RyDayType 
    {
        PREV_MONTH_DAY,     ///< 上月剩余日期
        NEXT_MONTH_DAY,     ///< 下个月的日期
        CURR_MONTH_DAY,     ///< 当月日期
        WEEKEND_DAY,        ///< 周末
        SELECT_DAY,         ///< 选中的日期
        CURRENT_DAY,        ///< 当前日期
    };

public:
    /// \brief 构造函数
    /// \param [in] parent 父控件
    explicit CRyDayLabel(QWidget* parent = NULL);

public:
    /// \brief 设置标签关联的日期
    /// \param [in] date 标签关联的日期
    void setDate(QDate date);

    /// \brief 获取标签关联的日期
    /// \return 标签关联的日期
    QDate getDate() const { return mDate; }

    /// \brief 
    /// \return 
    int getDayType() const { return mDateType; }

    /// \brief 设置
    /// \param [in] type
    void setDayType(int type);
    
    /// \brief 获取日期标签是否被选中
    /// \return 日期标签是否被选中
    bool getSelected() const { return mSelected; }

    /// \brief 设置日期标签是否被选中
    /// \param [in] value 日期标签是否被选中
    void setSelected(bool value);

    /// \brief 获取农历是否可见
    /// \return 农历是否可见
    bool getLunarVisible() const { return mLunarVisible; }

    /// \brief 设置农历是否可见
    /// \param [in] visible 农历是否可见
    void setLunarVisible(bool visible) { mLunarVisible = visible; }

signals:
    /// \brief 标签点击事件
    /// \param [in] date 标签日期
    void clicked(QDate date);

    /// \brief 标签双击事件
    /// \param [in] date 标签日期
    void doubleClicked(QDate date);

protected:
    /// 参考 QLabel::enterEvent(QEvent* event)
    virtual void enterEvent(QEvent* event);
    /// 参考 QLabel::leaveEvent(QEvent* event)
    virtual void leaveEvent(QEvent* event);
    /// 参考 QLabel::mousePressEvent(QMouseEvent* event)
    virtual void mousePressEvent(QMouseEvent* event);
    /// 参考 QLabel::mouseDoubleClickEvent(QMouseEvent* event)
    virtual void mouseDoubleClickEvent(QMouseEvent* event);
    /// 参考 QLabel::paintEvent(QPaintEvent* event)
    virtual void paintEvent(QPaintEvent* event);

private:
    /// 日期
    QDate mDate;
    /// 日期类型
    int mDateType;
    /// 鼠标是否悬浮
    bool mHover;
    /// 标签是否被选中
    bool mSelected;
    /// 农历是否可见
    bool mLunarVisible;
    /// 文本内容
    QString mTextConent;
};

/// \brief 日历控件
class CRyCalendarWidget : public QWidget
{
    Q_OBJECT
public:
    /// \brief 构造函数
    /// \param [in] sections 日期格式
    /// \param [in] parent 父控件
    explicit CRyCalendarWidget(QDateTimeEdit::Sections sections, QWidget* parent = NULL);

    /// \brief 析构函数
    ~CRyCalendarWidget();

public:
    /// \brief 获取选择的日期
    /// \return 选择的日期
    QDate getDate() const { return mSelectedDateTime.date(); }

    /// \brief 获取选择的日期时间
    /// \return 选择的日期时间
    QDateTime getDateTime() const { return mSelectedDateTime; }

    /// \brief 设置选择的日期
    /// \param [in] date 选择的日期
    void setDate(QDate date);

    /// \brief 获取选择的日期时间
    /// \return 选择的日期时间
    void setDateTime(QDateTime dateTime);

    /// \brief 设置日期格式
    /// \param [in] sections 日期格式
    void setDisplaySections(QDateTimeEdit::Sections sections);

    /// \brief 获取日期时间范围
    /// \param [in] minDateTime 最小日期
    /// \param [in] maxDateTime 最大日期
    void getDateTimeRange(QDateTime& minDateTime, QDateTime& maxDateTime);

    /// \brief 设置日期时间范围
    /// \param [out] minDateTime 最小日期
    /// \param [out] maxDateTime 最大日期
    void setDateTimeRange(const QDateTime& minDateTime, const QDateTime& maxDateTime);

signals:
    /// \brief 日期时间变化事件
    /// \param [in] dateTime 日期时间
    void dataTimeChanged(const QDateTime& dateTime);

private:
    /// \brief 更新控件
    void updateWidget();

    /// \brief 获取指定日期的标签对象
    /// \param [in] date 日期
    /// \return 指定日期的标签对象
    CRyDayLabel* getDayLabel(QDate date);

private Q_SLOTS:
    /// \brief 响应年、月组合框索引变化事件
    void onDateComboBoxIndexChanged(int);

    /// \brief 响应时、分、秒组合框索引变化事件
    void onTimeComboBoxIndexChanged(int);

    /// \brief 响应日期标签点击事件
    void onDayLabelClicked(QDate date); 
    
    /// \brief 响应日期标签双击击事件
    void onDayLabelDoubleClicked(QDate date);

    /// \brief 响应“今天”按钮点击事件
    void onTodayButtonClicked();

    /// \brief 响应“此刻”按钮点击事件
    void onNowButtonClicked();

    /// \brief 响应“确定”按钮点击事件
    void onOKButtonClicked();

private:
    /// 最小的日期
    QDateTime mMinDateTime;
    /// 最大的日期
    QDateTime mMaxDateTime;
    /// 选择的日期
    QDateTime mSelectedDateTime;
    /// 日期显示格式
    QDateTimeEdit::Sections mDisplaySections;

private:
    /// 年份组合框
    QComboBox* mYearComboBox;
    /// 月份组合框
    QComboBox* mMonthComboBox;
    /// 小时组合框
    QComboBox* mHourComboBox;
    /// 分钟组合框
    QComboBox* mMinComboBox;
    /// 秒组合框
    QComboBox* mSecComboBox;

    /// 小时后的分割控件
    QLabel* mHourSplitLabel;
    /// 小时后的分割控件
    QLabel* mMinuteSplitLabel;

    /// 星期控件
    QWidget* mWeekWidget;
    /// 日期列表控件
    QWidget* mDaysWidget;
    /// 日期控件集合
    CRyDayLabel* mDayLabels[42];

    /// “今天”按钮
    QPushButton* mTodayButton;
    /// “此刻”按钮
    QPushButton* mNowButton;
    /// “确定”按钮
    QPushButton* mOKButton;

    /// 当前选择的日期标签对象
    CRyDayLabel* mSelectedDayLabel;
};

#endif // _TESTDATEEDIT_CALENDARWIDGET_H_
