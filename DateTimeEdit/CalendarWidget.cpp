//////////////////////////////////////////////////////////////////////////
/// Copyright (C), 1998-2016, rytec Corporation. All rights reserved.
/// \file       CalendarWidget.cpp
/// \brief      
/// \author     wyf
/// \version    0.95
/// \date       2020-8
//////////////////////////////////////////////////////////////////////////

#include "CalendarWidget.h"
#include "DateTimeUtils.h"

#include <QStyledItemDelegate>
#include <QPushButton>
#include <QVBoxLayout>
#include <QMouseEvent>
#include <QComboBox>
#include <QPainter>

#pragma execution_character_set("utf-8")

CRyDayLabel::CRyDayLabel(QWidget *parent)
    : QLabel(parent)
    , mDateType(0)
    , mSelected(false)
    , mHover(false)
    , mLunarVisible(false)
{
}

void CRyDayLabel::setSelected(bool value)
{
    mSelected = value;
    update();
}

void CRyDayLabel::setDayType(int type)
{
    mDateType = type;
    update();
}

void CRyDayLabel::setDate(QDate date)
{
    mDate = date;
    mTextConent = QString::number(mDate.day());

    if (mLunarVisible)
    {
        QString lunarDate = CRyDateTimeUtils::getLunarDate(date.year(), date.month(), date.day());
        mTextConent.append("\n" + lunarDate);
    }
}

void CRyDayLabel::enterEvent(QEvent *e)
{
    if (PREV_MONTH_DAY == mDateType || NEXT_MONTH_DAY == mDateType)
        return;

    mHover = true;
    update();
    QLabel::enterEvent(e);
}

void CRyDayLabel::leaveEvent(QEvent *e)
{
    if (PREV_MONTH_DAY == mDateType || NEXT_MONTH_DAY == mDateType)
        return;

    mHover = false;
    update();
    QLabel::leaveEvent(e);
}

void CRyDayLabel::mousePressEvent(QMouseEvent* event)
{
    QLabel::mousePressEvent(event);
    emit clicked(mDate);
}

void CRyDayLabel::mouseDoubleClickEvent(QMouseEvent* event)
{
    QLabel::mouseDoubleClickEvent(event);
    emit doubleClicked(mDate);
}

void CRyDayLabel::paintEvent(QPaintEvent* event)
{
    Q_UNUSED(event);
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    // 不是悬停
    if (!mHover)
    {
        //  选中
        if (mSelected)
        {
            painter.setBrush(QBrush(QColor("#117dd9")));
            painter.setPen(QPen(QColor("#117dd9"), 1));

            if (mLunarVisible)
                painter.drawRect(rect());
            else
                painter.drawEllipse(rect().center(), 12, 12);

            painter.setPen(QPen(QColor("#ffffff"), 1));
            painter.drawText(rect(), Qt::AlignCenter, mTextConent);
        }
        else
        {
            //  当前日
            if (CURRENT_DAY == mDateType)
            {
                painter.setPen(QPen(QColor("#117dd9"), 1));
                if (mLunarVisible)
                    painter.drawRect(rect());
                else
                    painter.drawEllipse(rect().center(), 12, 12);
                painter.setPen(QPen(QColor("#cadfff"), 1));
                painter.drawText(rect(), Qt::AlignCenter, mTextConent);
            }
            // 其他月
            else if ((PREV_MONTH_DAY == mDateType) || (NEXT_MONTH_DAY == mDateType))
            {
                painter.setPen(QPen(QColor("#2360bc"), 1));
                painter.drawText(rect(), Qt::AlignCenter, mTextConent);
            }
            // 当前月
            else if (CURR_MONTH_DAY == mDateType)
            {
                painter.setPen(QPen(QColor("#cadfff"), 1));
                painter.drawText(rect(), Qt::AlignCenter, mTextConent);
            }
            // 周末
            else if (WEEKEND_DAY == mDateType)
            {
                painter.setPen(QPen(QColor("#00caff"), 1));
                painter.drawText(rect(), Qt::AlignCenter, mTextConent);
            }
        }
    }
    //  悬停的效果
    else
    {
        painter.setBrush(QBrush(QColor("#117dd9")));
        painter.setPen(QPen(QColor("#117dd9"), 1));
        if (mLunarVisible)
            painter.drawRect(rect());
        else
            painter.drawEllipse(rect().center(), 12, 12);
        painter.setPen(QPen(QColor("#ffffff"), 1));
        painter.drawText(rect(), Qt::AlignCenter, mTextConent);
    }
}


//////////////////////////////////////////////////////////////////////////
///
CRyCalendarWidget::CRyCalendarWidget(QDateTimeEdit::Sections sections, QWidget* parent)
    : QWidget(parent, Qt::Popup | Qt::FramelessWindowHint)
    , mYearComboBox(NULL)
    , mMonthComboBox(NULL)
    , mHourComboBox(NULL)
    , mMinComboBox(NULL)
    , mSecComboBox(NULL)
    , mHourSplitLabel(NULL)
    , mMinuteSplitLabel(NULL)
    , mWeekWidget(NULL)
    , mDaysWidget(NULL)
    , mTodayButton(NULL)
    , mNowButton(NULL)
    , mOKButton(NULL)
    , mSelectedDayLabel(NULL)
{
    mDisplaySections = sections;
    mMinDateTime = QDateTime(QDate(1900, 1, 1));
    mMaxDateTime = QDateTime(QDate(2099, 12, 31), QTime(23, 59, 59));
    mSelectedDateTime = QDateTime::currentDateTime();

    setAutoFillBackground(false);
    setAttribute(Qt::WA_TranslucentBackground, true);
    setStyleSheet(
        "QWidget#CentralWidget{"
        "    background-color:#EE013B6B;"
        "    border-radius:11px;"
        "}"
        ""
        "QPushButton{"
        "    color:#ffffff;"
        "    background-color:transparent;"
        "    border-radius:11px;"
        "    border:1px solid #259bf3;"
        "    font-size:12px;"
        "}"
        "QPushButton:hover{ background-color:rgba(37, 155, 243, 0.41); }"
        "QPushButton:pressed{ background-color:rgba(37, 155, 243, 0.99); }"
        ""
        "QComboBox{"
        "   color:#ffffff;"
        "   background-color:transparent;"
        "   selection-background-color: transparent;"
        "   font-size:14px;"
        "   min-height:20px;"
        "   selection-text-align:right;"
        "   border:0px solid"
        "}"
        "QComboBox::down-arrow{ image:url(:/Resources/ComboBoxArrow.png); }"
        "QComboBox::down-arrow:on{ top:1px; left:1px; }"
        "QComboBox::drop-down{ width:26px; border:none; }"
        "QComboBox QAbstractItemView{ border:none; outline:0px; }"
        "QComboBox QAbstractItemView::item{ height:26px; min-height:26px; }"
        "QComboBox QAbstractItemView::item:selected{ background-color:#0087f1; color:#e6ebf1; }"
        "QComboBox QAbstractItemView::item:!selected{ background-color:#e6ebf1; color:#000000; }");

    QVBoxLayout* calendarLayout = new QVBoxLayout(this);
    calendarLayout->setContentsMargins(0, 0, 0, 0);

    QWidget* centralWidget = new QWidget(this);
    centralWidget->setObjectName("CentralWidget");
    calendarLayout->addWidget(centralWidget);

    QVBoxLayout* centralLayout = new QVBoxLayout(centralWidget);
    centralLayout->setContentsMargins(0, 10, 0, 0);

    // 初始化标题控件
    {
        QWidget* titleWidget = new QWidget(this);
        titleWidget->setMinimumHeight(32);
        titleWidget->setStyleSheet("QWidget{ background-color:#061f65; }");
        centralLayout->addWidget(titleWidget);

        QHBoxLayout* titleWidgetLayout = new QHBoxLayout(titleWidget);
        titleWidgetLayout->setContentsMargins(14, 0, 14, 0);
        titleWidgetLayout->setSpacing(0);

        mYearComboBox = new QComboBox(this);
        QDate minDate = mMinDateTime.date();
        QDate maxDate = mMaxDateTime.date();
        for (int year = minDate.year(), i = 0; year <= maxDate.year(); year++, i++)
        {
            mYearComboBox->addItem(QString::number(year) + tr("年"));
            mYearComboBox->setItemData(i, year, Qt::UserRole);
        }
        mYearComboBox->setItemDelegate(new QStyledItemDelegate());
        titleWidgetLayout->addWidget(mYearComboBox);
        titleWidgetLayout->addStretch(1);

        QStringList monthList;
        monthList << tr("一月") << tr("二月") << tr("三月") << tr("四月") << tr("五月") << tr("六月")
            << tr("七月") << tr("八月") << tr("九月") << tr("十月") << tr("十一月") << tr("十二月");
        mMonthComboBox = new QComboBox(this);
        for (int i = 0; i < monthList.size(); i++)
        {
            mMonthComboBox->addItem(monthList[i]);
            mMonthComboBox->setItemData(i, i, Qt::UserRole);
        }
        mMonthComboBox->setItemDelegate(new QStyledItemDelegate());
        titleWidgetLayout->addWidget(mMonthComboBox);
        titleWidgetLayout->addStretch(1);

        mHourComboBox = new QComboBox(this);
        for (int i = 0; i < 24; i++)
        {
            mHourComboBox->addItem(QString::asprintf("%02d", i));
            mHourComboBox->setItemData(i, i, Qt::UserRole);
        }
        mHourComboBox->setItemDelegate(new QStyledItemDelegate());
        titleWidgetLayout->addWidget(mHourComboBox);

        mHourSplitLabel = new QLabel(tr(":"), this);
        mHourSplitLabel->setMinimumWidth(4);
        mHourSplitLabel->setStyleSheet("QLable{ color:#cadfff; font:bold 14px; }");
        titleWidgetLayout->addWidget(mHourSplitLabel);

        mMinComboBox = new QComboBox(this);
        for (int i = 0; i < 60; ++i)
        {
            mMinComboBox->addItem(QString::asprintf("%02d", i));
            mMinComboBox->setItemData(i, i, Qt::UserRole);
        }
        mMinComboBox->setItemDelegate(new QStyledItemDelegate());
        titleWidgetLayout->addWidget(mMinComboBox);

        mMinuteSplitLabel = new QLabel(tr(":"), this);
        mMinuteSplitLabel->setMinimumWidth(4);
        mMinuteSplitLabel->setStyleSheet("QLable{ color:#cadfff; font:bold 14px; }");
        titleWidgetLayout->addWidget(mMinuteSplitLabel);

        mSecComboBox = new QComboBox(this);
        for (int i = 0; i < 60; ++i)
        {
            mSecComboBox->addItem(QString::asprintf("%02d", i));
            mSecComboBox->setItemData(i, i, Qt::UserRole);
        }
        mSecComboBox->setItemDelegate(new QStyledItemDelegate());
        titleWidgetLayout->addWidget(mSecComboBox);
        titleWidgetLayout->addStretch(1);
    }

    // 初始化星期行
    {
        mWeekWidget = new QWidget(this);

        QHBoxLayout* weekWidgetLayout = new QHBoxLayout(mWeekWidget);
        weekWidgetLayout->setContentsMargins(0, 0, 0, 0);
        weekWidgetLayout->setSpacing(0);

        const static QString sWeekStrings[] = { tr("周日"), tr("周一"),
            tr("周二"), tr("周三"), tr("周四"), tr("周五"), tr("周六") };
        for (int i = 0; i < 7; i++)
        {
            QLabel* weekLabel = new QLabel(mWeekWidget);
            weekLabel->setText(sWeekStrings[i]);
            weekLabel->setMinimumHeight(30);
            weekLabel->setAlignment(Qt::AlignCenter);

            if ((0 == (i % 7)) || (6 == (i % 7)))
            {
                weekLabel->setStyleSheet("QLabel{ color:#00caff; font-size:12px; }");
            }
            else
            {
                weekLabel->setStyleSheet("QLabel{ color:#cadfff; font-size:12px; }");
            }
            weekWidgetLayout->addWidget(weekLabel);
        }

        centralLayout->addWidget(mWeekWidget);
    }

    // 初始化主体日期列表
    {
        mDaysWidget = new QWidget(this);
        QGridLayout* gridLayoutBody = new QGridLayout(mDaysWidget);
        gridLayoutBody->setHorizontalSpacing(0);
        gridLayoutBody->setVerticalSpacing(0);
        gridLayoutBody->setContentsMargins(0, 0, 0, 0);

        for (int i = 0; i < 42; i++)
        {
            mDayLabels[i] = new CRyDayLabel(mDaysWidget);
            mDayLabels[i]->setAlignment(Qt::AlignCenter);
            mDayLabels[i]->setText(QString::number(i));
            gridLayoutBody->addWidget(mDayLabels[i], i / 7, i % 7);
        }

        centralLayout->addWidget(mDaysWidget, 1);
    }

    // 初始化底部控件
    {
        QWidget* bottomWidget = new QWidget(this);
        QHBoxLayout* bottomWidgetLayout = new QHBoxLayout(bottomWidget);
        bottomWidgetLayout->setContentsMargins(25, 0, 25, 13);

        mTodayButton = new QPushButton(this);
        mTodayButton->setFixedSize(67, 23);
        mTodayButton->setText(tr("今天"));
        bottomWidgetLayout->addWidget(mTodayButton);
        bottomWidgetLayout->addStretch(1);

        mNowButton = new QPushButton(this);
        mNowButton->setText(tr("此刻"));
        mNowButton->setFixedSize(67, 23);
        bottomWidgetLayout->addWidget(mNowButton);
        bottomWidgetLayout->addStretch(1);

        mOKButton = new QPushButton(this);
        mOKButton->setFixedSize(67, 23);
        mOKButton->setText(tr("确定"));
        bottomWidgetLayout->addWidget(mOKButton);

        centralLayout->addWidget(bottomWidget);
    }

    updateWidget();

    // 关联事件
    connect(mYearComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(onDateComboBoxIndexChanged(int)));
    connect(mMonthComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(onDateComboBoxIndexChanged(int)));
    connect(mHourComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(onTimeComboBoxIndexChanged(int)));
    connect(mMinComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(onTimeComboBoxIndexChanged(int)));
    connect(mSecComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(onTimeComboBoxIndexChanged(int)));

    for (int i = 0; i < 42; i++)
    {
        connect(mDayLabels[i], SIGNAL(clicked(QDate)), this, SLOT(onDayLabelClicked(QDate)));
        connect(mDayLabels[i], SIGNAL(doubleClicked(QDate)), this, SLOT(onDayLabelDoubleClicked(QDate)));
    }

    connect(mTodayButton, SIGNAL(clicked()), this, SLOT(onTodayButtonClicked()));
    connect(mNowButton, SIGNAL(clicked()), this, SLOT(onNowButtonClicked()));
    connect(mOKButton, SIGNAL(clicked()), this, SLOT(onOKButtonClicked()));
}

CRyCalendarWidget::~CRyCalendarWidget()
{
}

CRyDayLabel* CRyCalendarWidget::getDayLabel(QDate date)
{
    for (int i = 0; i < 42; i++)
    {
        if (mDayLabels[i]->getDate() == date)
            return mDayLabels[i];
    }
    return NULL;
}

void CRyCalendarWidget::setDate(QDate date)
{
    setDateTime(QDateTime(date, mSelectedDateTime.time()));
}

void CRyCalendarWidget::setDateTime(QDateTime dateTime)
{
    mSelectedDateTime = dateTime;
    updateWidget();
    emit dataTimeChanged(mSelectedDateTime);
}

void CRyCalendarWidget::setDisplaySections(QDateTimeEdit::Sections sections)
{
    mDisplaySections = sections;
    updateWidget();
}

void CRyCalendarWidget::getDateTimeRange(QDateTime& min, QDateTime& max)
{
    min = mMinDateTime; max = mMaxDateTime;
}

void CRyCalendarWidget::setDateTimeRange(const QDateTime &min, const QDateTime &max)
{
    mMinDateTime = min; mMaxDateTime = max;
    updateWidget();
}

void CRyCalendarWidget::updateWidget()
{
    // 更新年、月、时、分、秒组合框
    QDate selectedDate = mSelectedDateTime.date();
    QTime selectedTime = mSelectedDateTime.time();
    mYearComboBox->blockSignals(true);
    mYearComboBox->setCurrentIndex(selectedDate.year() - mMinDateTime.date().year());
    mYearComboBox->blockSignals(false);

    mMonthComboBox->blockSignals(true);
    mMonthComboBox->setCurrentIndex(selectedDate.month() - 1);
    mMonthComboBox->blockSignals(false);

    mHourComboBox->blockSignals(true);
    mHourComboBox->setCurrentIndex(selectedTime.hour());
    mHourComboBox->blockSignals(false);

    mMinComboBox->blockSignals(true);
    mMinComboBox->setCurrentIndex(selectedTime.minute());
    mMinComboBox->blockSignals(false);

    mSecComboBox->blockSignals(true);
    mSecComboBox->setCurrentIndex(selectedTime.second());
    mSecComboBox->blockSignals(false);

    // 设置日期标签列表
    int selectedMonth = selectedDate.month();
    int preMonth = selectedMonth == 1 ? 12 : selectedMonth - 1;

    QDate beginDate(selectedDate.year(), selectedDate.month(), 1);
    int firstDayWeek = beginDate.dayOfWeek();
    beginDate = beginDate.addDays(firstDayWeek == 0 ? -7 : -firstDayWeek);
    for (int i = 0; i < 42; i++)
    {
        QDate tempDate = beginDate.addDays(i);
        mDayLabels[i]->setDate(tempDate);
        mDayLabels[i]->setDayType(tempDate.month() == selectedMonth ? CRyDayLabel::CURR_MONTH_DAY :
            tempDate.month() == preMonth ? CRyDayLabel::PREV_MONTH_DAY : CRyDayLabel::NEXT_MONTH_DAY);
    }

    // 根据显示格式设置响应控件的可见性
    mYearComboBox->setVisible(mDisplaySections & QDateTimeEdit::YearSection);
    mMonthComboBox->setVisible(mDisplaySections & QDateTimeEdit::MonthSection);
    mWeekWidget->setVisible(mDisplaySections & QDateTimeEdit::DaySection);
    mDaysWidget->setVisible(mDisplaySections & QDateTimeEdit::DaySection);

    mHourComboBox->setVisible(mDisplaySections & QDateTimeEdit::HourSection);
    mHourSplitLabel->setVisible(mDisplaySections & QDateTimeEdit::HourSection);
    mMinComboBox->setVisible(mDisplaySections & QDateTimeEdit::MinuteSection);
    mMinuteSplitLabel->setVisible(mDisplaySections & QDateTimeEdit::SecondSection);
    mSecComboBox->setVisible(mDisplaySections & QDateTimeEdit::SecondSection);

    mTodayButton->setVisible(mDisplaySections & QDateTimeEdit::DateSections_Mask);
    mNowButton->setVisible(mDisplaySections & QDateTimeEdit::TimeSections_Mask);

    // 设置选择的日期控件
    if (NULL != mSelectedDayLabel)
        mSelectedDayLabel->setSelected(false);
    mSelectedDayLabel = getDayLabel(mSelectedDateTime.date());
    if (NULL != mSelectedDayLabel)
        mSelectedDayLabel->setSelected(true);

    // 重新计算控件大小
    int minWidth = 350;
    if (!(mDisplaySections & QDateTimeEdit::DaySection))
    {
        if (!(mDisplaySections & QDateTimeEdit::YearSection))
            minWidth -= 80;
        if (!(mDisplaySections & QDateTimeEdit::MonthSection))
            minWidth -= 80;
        if (!(mDisplaySections & QDateTimeEdit::HourSection))
            minWidth -= 50;
        if (!(mDisplaySections & QDateTimeEdit::MinuteSection))
            minWidth -= 50;
        if (!(mDisplaySections & QDateTimeEdit::SecondSection))
            minWidth -= 50;
    }
    int minHeight = 300 - (mDisplaySections & QDateTimeEdit::DaySection ? 0 : 220);
    setMinimumSize(minWidth, minHeight);
}

void CRyCalendarWidget::onDayLabelClicked(QDate date)
{
    setDateTime(QDateTime(date, mSelectedDateTime.time()));
}

void CRyCalendarWidget::onDayLabelDoubleClicked(QDate date)
{
    setDateTime(QDateTime(date, mSelectedDateTime.time()));
    close();
}

void CRyCalendarWidget::onDateComboBoxIndexChanged(int)
{
    int year = mMinDateTime.date().year() + mYearComboBox->currentIndex();
    int month = mMonthComboBox->currentIndex() + 1;
    setDate(QDate(year, month, mSelectedDateTime.date().day()));
}

void CRyCalendarWidget::onTimeComboBoxIndexChanged(int)
{
    QTime time(mHourComboBox->currentIndex(), mMinComboBox->currentIndex(), mSecComboBox->currentIndex());
    setDateTime(QDateTime(mSelectedDateTime.date(), time));
}

void CRyCalendarWidget::onTodayButtonClicked()
{
    setDateTime(QDateTime(QDate::currentDate(), mSelectedDateTime.time()));
}

void CRyCalendarWidget::onNowButtonClicked()
{
    setDateTime(QDateTime::currentDateTime());
}

void CRyCalendarWidget::onOKButtonClicked()
{
    close();
}
