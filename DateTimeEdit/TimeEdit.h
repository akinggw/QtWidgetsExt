//////////////////////////////////////////////////////////////////////////
/// Copyright (C), 1998-2016, rytec Corporation. All rights reserved.
/// \file       TimeEdit.h
/// \brief      
/// \author     wyf
/// \version    0.95
/// \date       2020-8
//////////////////////////////////////////////////////////////////////////
#ifndef _TESTDATEEDIT_TIMEEDIT_H_
#define _TESTDATEEDIT_TIMEEDIT_H_

#include <QTimeEdit>

class CRyCalendarWidget;
class QPushButton;

/// \brief 时间控件
class CRyTimeEdit : public QTimeEdit
{
    Q_OBJECT
public:
    /// \brief 构造函数
    /// \param [in] parent 父控件对象
    explicit CRyTimeEdit(QWidget* parent = NULL);

    /// \brief 析构函数
    ~CRyTimeEdit();

public:
    /// \brief 设置事件格式
    /// \param [in] format 时间格式
    void setDisplayFormat(const QString& format);

protected Q_SLOTS:
    /// \brief 响应弹出日历控件按钮点击事件
    void onPopupButtonClicked();

    /// \brief 响应日历控件点击日期事件
    void onDateTimeChanged(const QDateTime& dateTime);

private:
    /// 弹出日历控件按钮
    QPushButton* mPopupButton;
    /// 日历控件
    CRyCalendarWidget* mCalendarWidget;
};

#endif // _TESTDATEEDIT_TIMEEDIT_H_
