//////////////////////////////////////////////////////////////////////////
/// Copyright (C), 1998-2016, rytec Corporation. All rights reserved.
/// \file       DateTimeEdit.h
/// \brief      
/// \author     wyf
/// \version    0.95
/// \date       2020-8
//////////////////////////////////////////////////////////////////////////

#ifndef _TESTDATEEDIT_DATETIMEEDIT_H_
#define _TESTDATEEDIT_DATETIMEEDIT_H_

#include <QDateTimeEdit>
#include <QPushButton>

class CRyCalendarWidget;

/// \brief 时间日期控件
class CRyDateTimeEdit : public QDateTimeEdit
{
    Q_OBJECT
public:
    /// \brief 构造函数
    /// \param [in] parent 父控件对象
    explicit CRyDateTimeEdit(QWidget* parent = NULL);

    /// \param 析构函数
    ~CRyDateTimeEdit();

public:
    /// \brief 设置显示格式
    /// \param [in] format 显示格式
    void setDisplayFormat(const QString& format);

protected Q_SLOTS:
    /// \brief 响应弹出日历控件按钮点击事件
    void onPopupButtonClicked();

    /// \brief 响应日历控件点击日期事件
    void onDateTimeChanged(const QDateTime& dateTime);

private:
    /// 弹出日历控件按钮
    QPushButton* mPopupButton;
    /// 日历控件
    CRyCalendarWidget* mCalendarWidget;
};

#endif // _TESTDATEEDIT_ZDATETIMEEDIT_H_
