//////////////////////////////////////////////////////////////////////////
/// Copyright (C), 1998-2016, rytec Corporation. All rights reserved.
/// \file       MainWindow.h
/// \brief      
/// \author     wyf
/// \version    0.95
/// \date       2020-8
//////////////////////////////////////////////////////////////////////////

#ifndef _TESTDATEEDIT_MAINWINDOW_H_
#define _TESTDATEEDIT_MAINWINDOW_H_

#include <QMainWindow>

class Ui_MainWindow;

/// \brief 主窗体
class CRyMainWindow : public QMainWindow
{
    Q_OBJECT
public:
    /// \brief 构造函数
    /// \param [in] parent 父对象
    CRyMainWindow(QWidget* parent = NULL);

    /// \brief 析构函数
    ~CRyMainWindow();

private:
    /// Ui对象
    Ui_MainWindow* mUi;
};
#endif // _TESTDATEEDIT_MAINWINDOW_H_
