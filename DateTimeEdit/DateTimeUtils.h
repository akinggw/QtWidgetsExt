//////////////////////////////////////////////////////////////////////////
/// Copyright (C), 1998-2016, rytec Corporation. All rights reserved.
/// \file       DateTimeUtils.h
/// \brief      
/// \author     wyf
/// \version    0.95
/// \date       2020-8
//////////////////////////////////////////////////////////////////////////
#ifndef _DATETIMEEDIT_DATETIMEUTILS_H_
#define _DATETIMEEDIT_DATETIMEUTILS_H_

#include <QtGlobal>
#include <QString>

/// \brief 日期工具类
class CRyDateTimeUtils
{
public:
    /// \brief 计算农历日期
    /// \param [in] year 年份
    /// \param [in] month 月份
    /// \param [in] day 日期
    /// \return 农历日期
    static QString getLunarDate(int year, int month, int day);

private:
    /// \brief 
    /// \param [in] month
    /// \param [in] day
    /// \return 
    static QString holiday(int month, int day);

    /// \brief 
    /// \param [in] year
    /// \param [in] month
    /// \param [in] day
    /// \return 
    static QString solarTerms(int year, int month, int day);

    /// \brief 
    /// \param [in] month
    /// \param [in] day
    /// \return 
    static QString lunarFestival(int month, int day);
};

#endif // _DATETIMEEDIT_DATETIMEUTILS_H_
