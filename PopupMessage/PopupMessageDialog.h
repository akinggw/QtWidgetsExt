﻿//////////////////////////////////////////////////////////////////////////
/// Copyright (C), 1998-2016, rytec Corporation. All rights reserved.
/// \file       PopupMessageDialog.h
/// \brief      
/// \author     wyf
/// \version    0.95
/// \date       2021-4
//////////////////////////////////////////////////////////////////////////

#ifndef _SYSTEMMESSAGE_POPUPMESSAGEDIALOG_H_
#define _SYSTEMMESSAGE_POPUPMESSAGEDIALOG_H_

#include <QtWidgets/QDialog>

class QPropertyAnimation;
class QPlainTextEdit;
class QPushButton;
class QLabel;
class QTimer;

/// \brief 系统信息控件
class CRyPopupMessageDialog : public QDialog
{
    Q_OBJECT
public:
    /// \brief 构造函数
    /// \param [in] parent 父对象
    CRyPopupMessageDialog(QWidget* parent = NULL);

    /// \brief 析构函数
    ~CRyPopupMessageDialog();

public:
    /// \brief 设置信息对话框的标题和图标
    /// \param [in] title 信息对话框的标题
    /// \param [in] icon 信息对话框的图标
    void setTitle(const QString& title, QIcon icon);

    /// \brief 设置信息对话框的内容
    /// \param [in] message 信息对话框的内容
    void setMessage(const QString& message);

    /// \brief 设置自动关闭的时长，当时长为0时不再自动关闭
    /// \param [in] msecond 自动关闭的时长
    void setAutoCloseTime(int msecond);

    /// \brief 显示信息对话框
    void showMessage();

private Q_SLOTS:
    /// \brief 
    void onCloseTimeout();

    /// \brief 
    void onAnimationFinished();
    
    /// \brief 
    void onCloseButtonClicked();

private:
    /// 菜单移动动画效果
    QPropertyAnimation* mGeomAnimation;

    /// 自动关闭计时器
    QTimer* mCloseTimer;

    /// 标题标签控件
    QLabel* mTitleLabel;
    
    /// 图标按钮控件
    QPushButton* mIconButton;
    
    /// 关闭按钮控件
    QPushButton* mTitleCloseButton;

    /// 信息标签控件
    QPlainTextEdit* mMessageEdit;
    
    /// 关闭按钮控件
    QPushButton* mCloseButton;

    /// 自动关闭的时长
    int mAutoCloseTime;
};

#endif // _SYSTEMMESSAGE_POPUPMESSAGEDIALOG_H_