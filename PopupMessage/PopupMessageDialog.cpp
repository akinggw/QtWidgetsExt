﻿//////////////////////////////////////////////////////////////////////////
/// Copyright (C), 1998-2016, rytec Corporation. All rights reserved.
/// \file       PopupMessageDialog.cpp
/// \brief      
/// \author     wyf
/// \version    0.95
/// \date       2021-4
//////////////////////////////////////////////////////////////////////////
#include "PopupMessageDialog.h"

#include <QPropertyAnimation>
#include <QDesktopWidget>
#include <QApplication>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QPlainTextEdit>
#include <QTimer>
#include <QIcon>
#include <QLabel>

#if defined(Q_CC_MSVC) && (Q_CC_MSVC >= 1600)
#pragma execution_character_set("utf-8")
#endif // RY_CC_MSVC

CRyPopupMessageDialog::CRyPopupMessageDialog(QWidget *parent)
    : QDialog(parent)
    , mGeomAnimation(new QPropertyAnimation(this))
    , mCloseTimer(new QTimer(this))
    , mTitleLabel(NULL)
    , mIconButton(NULL)
    , mTitleCloseButton(NULL)
    , mMessageEdit(NULL)
    , mCloseButton(NULL)
    , mAutoCloseTime(5000)
{
    setFixedSize(300, 200);
    setWindowFlags(Qt::WindowMinMaxButtonsHint | Qt::FramelessWindowHint);

    // 创建并设置标题相关控件
    mIconButton = new QPushButton(this);
    mIconButton->setMaximumHeight(25);
    mIconButton->setStyleSheet("QPushButton { border-style:none; }");
    mIconButton->setIcon(QIcon(":/Resources/Title.png"));

    mTitleLabel = new QLabel(tr("提示"), this);
    mTitleLabel->setMaximumHeight(25);

    mTitleCloseButton = new QPushButton(this);
    mTitleCloseButton->setFixedSize(QSize(24, 24));
    mTitleCloseButton->setStyleSheet(
        "QPushButton{"                                                                  \
        "    border-image:url(:/Resources/Close.png);"                                  \
        "    border-width:0px;"                                                         \
        "}"                                                                             \
        "QPushButton:hover{"                                                            \
        "    border-image:url(:/Resources/CloseHover.png);"                             \
        "    border-width:0px;"                                                         \
        "}"                                                                             \
        "QPushButton:pressed{"                                                          \
        "    border-image:url(:/Resources/CloseHover.png);"                             \
        "    border-width:0px;"                                                         \
        "}");

    QHBoxLayout* tiltleLayout = new QHBoxLayout();
    tiltleLayout->addWidget(mIconButton);
    tiltleLayout->addWidget(mTitleLabel);
    tiltleLayout->addStretch();
    tiltleLayout->addWidget(mTitleCloseButton);
    tiltleLayout->setMargin(0);

    // 创建并设置信息内容控件
    mMessageEdit = new QPlainTextEdit(tr(""), this);
    mMessageEdit->setPlainText(tr("你好，弹出信息对话框!"));
    mMessageEdit->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    mMessageEdit->setFrameShape(QFrame::NoFrame);
    mMessageEdit->setReadOnly(true);

    QPalette palette = mMessageEdit->palette();
    palette.setBrush(QPalette::Base, QBrush(QColor(255, 0, 0, 0)));
    mMessageEdit->setPalette(palette);

    QHBoxLayout* messageLayout = new QHBoxLayout();
    messageLayout->setContentsMargins(5, 5, 5, 5);
    messageLayout->addWidget(mMessageEdit);

    // 创建底部控件
    mCloseButton = new QPushButton(tr("关闭"), this);
    mCloseButton->setStyleSheet("QPushButton{ border-style:none; } QPushButton:hover{ color:#16EFCC; }");

    QHBoxLayout* bottomLayout = new QHBoxLayout();
    bottomLayout->setContentsMargins(5, 5, 5, 5);
    bottomLayout->addStretch();
    bottomLayout->addWidget(mCloseButton);
   
    QVBoxLayout* mainLayout = new QVBoxLayout();
    mainLayout->setContentsMargins(5, 5, 5, 5);
    mainLayout->addLayout(tiltleLayout);
    mainLayout->addLayout(messageLayout);
    mainLayout->addLayout(bottomLayout);
    setLayout(mainLayout);

    connect(mCloseTimer, SIGNAL(timeout()), this, SLOT(onCloseTimeout()));
    connect(mCloseButton, SIGNAL(clicked()), this, SLOT(onCloseButtonClicked()));
    connect(mTitleCloseButton, SIGNAL(clicked()), this, SLOT(onCloseButtonClicked()));
    connect(mGeomAnimation, SIGNAL(finished()), this, SLOT(onAnimationFinished()));
}

CRyPopupMessageDialog::~CRyPopupMessageDialog()
{

}

void CRyPopupMessageDialog::showMessage()
{
    // 设置弹出动画
    QDesktopWidget* desktopWidget = QApplication::desktop();
    QRect rect = desktopWidget->availableGeometry();
    QPoint startPos(rect.right() - width(), rect.bottom());
    QPoint endPos(rect.right() - width(), rect.bottom() - height());

    mGeomAnimation->setProperty("AnimationType", 0);
    mGeomAnimation->setTargetObject(this);
    mGeomAnimation->setPropertyName("pos");
    mGeomAnimation->setDuration(1000);
    mGeomAnimation->setStartValue(startPos);
    mGeomAnimation->setEndValue(endPos);
    mGeomAnimation->start();

    // 启动自动关闭计时器
    if (mAutoCloseTime > 0)
    {
        mCloseTimer->setSingleShot(true);
        mCloseTimer->start(mAutoCloseTime);
    }

    // 显示信息控件
    show();
}

void CRyPopupMessageDialog::setTitle(const QString& title, QIcon icon)
{
    mIconButton->setIcon(icon);
    mTitleLabel->setText(title);
}

void CRyPopupMessageDialog::setMessage(const QString& message)
{
    mMessageEdit->setPlainText(message);
}

void CRyPopupMessageDialog::setAutoCloseTime(int msecond)
{
    mAutoCloseTime = msecond;
}

void CRyPopupMessageDialog::onCloseTimeout()
{
    // 设置隐藏动画
    QDesktopWidget* desktopWidget = QApplication::desktop();
    QRect rect = desktopWidget->availableGeometry();
    QPoint endPos(rect.right() - width(), rect.bottom());
    QPoint startPos(rect.right() - width(), rect.bottom() - height());

    mGeomAnimation->setProperty("AnimationType", 1);
    mGeomAnimation->setTargetObject(this);
    mGeomAnimation->setPropertyName("pos");
    mGeomAnimation->setDuration(1000);
    mGeomAnimation->setStartValue(startPos);
    mGeomAnimation->setEndValue(endPos);
    mGeomAnimation->start();
}

void CRyPopupMessageDialog::onAnimationFinished()
{
    QPropertyAnimation* animation = dynamic_cast<QPropertyAnimation*>(sender());
    if (NULL == animation)
        return;

    QVariant variant = animation->property("AnimationType");
    if (!variant.isValid() || variant.type() != QVariant::Int)
        return;

    if (variant.value<int>() != 1)
        return;

    this->close();
}

void CRyPopupMessageDialog::onCloseButtonClicked()
{
    mGeomAnimation->stop();
    mCloseTimer->stop();
    this->close();
}
