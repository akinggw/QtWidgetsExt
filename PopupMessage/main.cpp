﻿//////////////////////////////////////////////////////////////////////////
/// Copyright (C), 1998-2016, rytec Corporation. All rights reserved.
/// \file       main.cpp
/// \brief      
/// \author     wyf
/// \version    0.95
/// \date       2021-4
//////////////////////////////////////////////////////////////////////////

#include <QtWidgets/QApplication>
#include "PopupMessageDialog.h"

int main(int argc, char *argv[])
{
    QApplication application(argc, argv);

    CRyPopupMessageDialog dialog;
    dialog.showMessage();

    return application.exec();
}
