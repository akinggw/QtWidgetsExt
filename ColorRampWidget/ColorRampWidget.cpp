//////////////////////////////////////////////////////////////////////////
/// Copyright (C), 1998-2016, rytec Corporation. All rights reserved.
/// \file       ColorRampWidget.cpp
/// \brief      
/// \author     wyf
/// \version    0.95
/// \date       2020-9
//////////////////////////////////////////////////////////////////////////

#include "ColorRampWidget.h"

#include "ui_ColorRampWidget.h"
#include <QResizeEvent>
#include <QPaintEvent>
#include <QMouseEvent>
#include <QPainter>

#define TOP_BOTTOM_HEIGHT   18

CRyColorRampWidget::CRyColorRampWidget(QWidget *parent)
    : QWidget(parent)
    , mUi(new Ui_ColorRampWidget())
    , mMousePressed(false)
    , mSelectedBotSlider(-1)
    , mSelectedTopSlider(-1)
{
    mUi->setupUi(this);
}

CRyColorRampWidget::~CRyColorRampWidget()
{
    delete mUi;
}

void CRyColorRampWidget::mouseMoveEvent(QMouseEvent* event)
{
    if ((mSelectedTopSlider != -1 && mSelectedBotSlider != -1) || !mMousePressed)
        return;

    if (mSelectedTopSlider != -1)
    {
        // 修改滑块位置
        QPair<int, QColor> slider = mTopSliders[mSelectedTopSlider];
        slider.first = event->pos().x() - mTopSliderRect.left();
        slider.first = std::max(slider.first, 0);
        slider.first = std::min(slider.first, mBotSliderRect.width());

        // 对滑块集合进行排序
        mTopSliders.removeAt(mSelectedTopSlider);
        mSelectedTopSlider = -1;
        for (int i = 1; i < mTopSliders.size(); i++)
        {
            if (slider.first >= mTopSliders[i - 1].first && slider.first <= mTopSliders[i].first)
            {
                mTopSliders.insert(i, slider);
                mSelectedTopSlider = i;
                break;
            }
        }
        if (mSelectedTopSlider == -1)
        {
            if (mTopSliders.size() > 0 && slider.first < mTopSliders[0].first)
            {
                mTopSliders.insert(0, slider);
                mSelectedTopSlider = 0;
            }
            else
            {
                mTopSliders.append(slider);
                mSelectedTopSlider = mTopSliders.size() - 1;
            }
        }
    }

    if (mSelectedBotSlider != -1)
    {
        // 修改滑块位置
        QPair<int, QColor> slider = mBotSliders[mSelectedBotSlider];
        slider.first = event->pos().x() - mBotSliderRect.left();
        slider.first = std::max(slider.first, 0);
        slider.first = std::min(slider.first, mBotSliderRect.width());

        // 对滑块集合进行排序
        mBotSliders.removeAt(mSelectedBotSlider);
        mSelectedBotSlider = -1;
        for (int i = 1; i < mBotSliders.size(); i++)
        {
            if (slider.first >= mBotSliders[i - 1].first && slider.first <= mBotSliders[i].first)
            {
                mBotSliders.insert(i, slider);
                mSelectedBotSlider = i;
                break;
            }
        }
        if (mSelectedBotSlider == -1)
        {
            if (mBotSliders.size() > 0 && slider.first < mBotSliders[0].first)
            {
                mBotSliders.insert(0, slider);
                mSelectedBotSlider = 0;
            }
            else
            {
                mBotSliders.append(slider);
                mSelectedBotSlider = mBotSliders.size() - 1;
            }
        }
    }
    update();
}

void CRyColorRampWidget::mousePressEvent(QMouseEvent* event)
{
    if (event->button() != Qt::LeftButton)
        return;

    mMousePressed = true;
    QPoint mousePos = event->pos();
    if (mTopSliderRect.contains(mousePos))
    {
        mSelectedBotSlider = -1;
        // 判断是否击中上方的滑块
        for (int i = 0; i < mTopSliders.size(); i++)
        {
            QPoint pos(mTopSliderRect.left() + mTopSliders[i].first, mTopSliderRect.bottom());
            QRect rect(pos.x() - 5, pos.y() - 10, 10, 15);
            if (rect.contains(mousePos))
            {
                mSelectedTopSlider = i;
                update();
                return;
            }
        }

        // 添加滑块
        int index = 0;
        int pos = mousePos.x() - mTopSliderRect.left();
        for (int i = 0; i < mTopSliders.size(); i++)
        {
            if (pos > mTopSliders[i].first)
                index++;
        }
#if 1
        QColor clr(std::rand() / (double)RAND_MAX * 255, std::rand() / (double)RAND_MAX * 255,
            std::rand() / (double)RAND_MAX * 255, std::rand() / (double)RAND_MAX * 255);
        mTopSliders.insert(index, QPair<int, QColor>(pos, clr));
#else
        mTopSliders.insert(index, QPair<int, QColor>(pos, QColor(0, 0, 0)));
#endif
        mSelectedTopSlider = index;
        update();
    }
    else if (mCorlorRampRect.contains(mousePos))
    {
        // 获取颜色，并将其赋值给当前滑块
    }
    else if (mBotSliderRect.contains(mousePos))
    {
        mSelectedTopSlider = -1;
        // 判断是否击中下方的滑块
        for (int i = 0; i < mBotSliders.size(); i++)
        {
            QPoint pos(mBotSliderRect.left() + mBotSliders[i].first, mBotSliderRect.top());
            QRect rect(pos.x() - 5, pos.y(), 10, 15);
            if (rect.contains(event->pos()))
            {
                mSelectedBotSlider = i;
                update();
                return;
            }
        }

        // 添加滑块
        int index = 0;
        int pos = mousePos.x() - mBotSliderRect.left();
        for (int i = 0; i < mBotSliders.size(); i++)
        {
            if (pos > mBotSliders[i].first)
                index++;
        }
#if 1
        QColor clr(std::rand() / (double)RAND_MAX * 255, std::rand() / (double)RAND_MAX * 255,
            std::rand() / (double)RAND_MAX * 255, std::rand() / (double)RAND_MAX * 255);
        mBotSliders.insert(index, QPair<int, QColor>(pos, clr));
#else
        mBotSliders.insert(index, QPair<int, QColor>(pos, QColor(0, 0, 0)));
#endif
        mSelectedBotSlider = index;
        update();
    }
    else
    {
        mSelectedTopSlider = -1;
        mSelectedBotSlider = -1;
        update();
    }
}

void CRyColorRampWidget::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() != Qt::LeftButton)
        return;
    mMousePressed = false;
}

void CRyColorRampWidget::paintEvent(QPaintEvent*)
{
    QPainter painter(this);
    // 绘制上方的滑块
    painter.setPen(QPen(QColor(112, 112, 112)));
    for (int i = 0; i < mTopSliders.size(); i++)
    {
        QPoint pos(mTopSliderRect.left() + mTopSliders[i].first, mTopSliderRect.bottom());
        QPainterPath triangle = calcTriangle(pos, false);
        painter.drawPath(triangle);
        if (mSelectedTopSlider == i)
            painter.fillPath(triangle, QBrush(QColor(0, 0, 0)));

        QRect rect(pos.x() - 5, pos.y() - 15, 10, 10);
        painter.drawRect(rect);

        rect.adjust(2, 2, -1, -1);
        painter.fillRect(rect, QBrush(mTopSliders[i].second));
    }

    // 绘制色带背景及色带
    for (int j = mCorlorRampRect.top(), row = 0; j <= mCorlorRampRect.bottom(); j += 4, row++)
    {
        int height = std::min(mCorlorRampRect.bottom() - j, 4);
        for (int i = mCorlorRampRect.left(), col = 0; i <= mCorlorRampRect.right(); i += 4, col++)
        {
            int width = std::min(mCorlorRampRect.right() - i, 4);
            QColor clr = ((row + col) % 2 == 0) ? QColor(255, 255, 255) : QColor(204, 204, 204);
            painter.fillRect(i, j, width, height, QBrush(clr));
        }
    }
    if (!mBotSliders.empty())
    {
        int leftSlider = 0, rightSilder = 0;
        for (int i = mCorlorRampRect.left(); i <= mCorlorRampRect.right(); i++)
        {
            if (i > mBotSliders[rightSilder].first)
            {
                leftSlider = rightSilder;
                rightSilder++;
            }
            rightSilder = std::min(rightSilder, mBotSliders.size() - 1);
            
            QColor color;
            int dis = mBotSliders[rightSilder].first - mBotSliders[leftSlider].first;
            if (dis <= 0)
            {
                color = mBotSliders[leftSlider].second;
            }
            else
            {
                double ratio = (i - mBotSliders[leftSlider].first) / double(dis);
                QColor leftClr = mBotSliders[leftSlider].second;
                QColor rightClr = mBotSliders[rightSilder].second;
                color.setRed(leftClr.red() + (rightClr.red() - leftClr.red()) * ratio);
                color.setGreen(leftClr.green() + (rightClr.green() - leftClr.green()) * ratio);
                color.setBlue(leftClr.blue() + (rightClr.blue() - leftClr.blue()) * ratio);
                color.setAlpha(leftClr.alpha() + (rightClr.alpha() - leftClr.alpha()) * ratio);
            }
            painter.setPen(QPen(color));
            painter.drawLine(i, mCorlorRampRect.top(), i, mCorlorRampRect.bottom());
        }
    }
    painter.setPen(QPen(QColor(158, 158, 158)));
    painter.drawRect(mCorlorRampRect);
    
    // 绘制下方的滑块
    painter.setPen(QPen(QColor(112, 112, 112)));
    for (int i = 0; i < mBotSliders.size(); i++)
    {
        QPoint pos(mBotSliderRect.left() + mBotSliders[i].first, mBotSliderRect.top());
        QPainterPath triangle = calcTriangle(pos, true);
        painter.drawPath(triangle);
        if (mSelectedBotSlider == i)
            painter.fillPath(triangle, QBrush(QColor(0, 0, 0)));

        QRect rect(pos.x() - 5, pos.y() + 5, 10, 10);;
        painter.drawRect(rect);

        rect.adjust(2, 2, -1, -1);
        painter.fillRect(rect, QBrush(mBotSliders[i].second));
    }
}

void CRyColorRampWidget::resizeEvent(QResizeEvent* event)
{
    QSize size = event->size();
    mTopSliderRect = QRect(5, 0, size.width() - 10, TOP_BOTTOM_HEIGHT);
    mBotSliderRect = QRect(5, size.height() - TOP_BOTTOM_HEIGHT, size.width() - 10, TOP_BOTTOM_HEIGHT);
    mCorlorRampRect = QRect(5, TOP_BOTTOM_HEIGHT, size.width() - 10, size.height() - TOP_BOTTOM_HEIGHT * 2);
}

QPainterPath CRyColorRampWidget::calcTriangle(QPoint pos, bool up)
{
    QPainterPath path;
    path.moveTo(pos);
    if (up)
    {
        path.lineTo(QPoint(pos.x() - 5, pos.y() + 5));
        path.lineTo(QPoint(pos.x() + 5, pos.y() + 5));
    }
    else
    {
        path.lineTo(QPoint(pos.x() + 5, pos.y() - 5));
        path.lineTo(QPoint(pos.x() - 5, pos.y() - 5));
    }
    path.lineTo(pos);
    return path;
}
