//////////////////////////////////////////////////////////////////////////
/// Copyright (C), 1998-2016, rytec Corporation. All rights reserved.
/// \file       ColorRampWidget.h
/// \brief      
/// \author     wyf
/// \version    0.95
/// \date       2020-9
//////////////////////////////////////////////////////////////////////////

#ifndef _COLORRAMPWIDGET_COLORRAMPWIDGET_H_
#define _COLORRAMPWIDGET_COLORRAMPWIDGET_H_

#include <QWidget>

class Ui_ColorRampWidget;

/// \brief 色带控件
class CRyColorRampWidget : public QWidget
{
    Q_OBJECT
public:
    /// \brief 构造函数
    /// \param [in] parent 父对象
    explicit CRyColorRampWidget(QWidget* parent = NULL);

    /// \brief 析构函数
    ~CRyColorRampWidget();

private:
    /// 参考 \ref QWidget::mouseMoveEvent(QMouseEvent* event)
    virtual void mouseMoveEvent(QMouseEvent* event);

    /// 参考 \ref QWidget::mousePressEvent(QMouseEvent* event)
    virtual void mousePressEvent(QMouseEvent* event);
   
    /// 参考 \ref QWidget::mouseReleaseEvent(QMouseEvent* event)
    virtual void mouseReleaseEvent(QMouseEvent* event);

    /// 参考 \ref QWidget::paintEvent(QPaintEvent* event)
    virtual void paintEvent(QPaintEvent* event);

    /// 参考 \ref QWidget::resizeEvent(QResizeEvent* event)
    virtual void resizeEvent(QResizeEvent* event);

private:
    /// \brief 计算三角形的绘制路径
    /// \param [in] pos 顶点位置
    /// \param [in] up 三角形是否向上
    /// \return 三角形的绘制路径
    QPainterPath calcTriangle(QPoint pos, bool up);

private:
    /// Ui对象
    Ui_ColorRampWidget* mUi;

    /// 鼠标是否按下
    bool mMousePressed;

    /// 色带的矩形区域
    QRect mCorlorRampRect;

    /// 顶部滑块的矩形区域
    QRect mTopSliderRect;

    /// 底部滑块的矩形区域
    QRect mBotSliderRect;

    /// 选中的顶部滑块索引
    int mSelectedTopSlider;

    /// 选中的底部滑块索引
    int mSelectedBotSlider;

    /// 顶部滑块集合
    QVector< QPair<int, QColor> > mTopSliders;

    /// 底部滑块集合
    QVector< QPair<int, QColor> > mBotSliders;
};

#endif // _COLORRAMPWIDGET_COLORRAMPWIDGET_H_
