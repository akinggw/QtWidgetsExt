//////////////////////////////////////////////////////////////////////////
/// Copyright (C), 1998-2016, rytec Corporation. All rights reserved.
/// \file       main.cpp
/// \brief      
/// \author     WYF
/// \version    0.95
/// \date       2017-5
//////////////////////////////////////////////////////////////////////////

#include "ColorRampWidget.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    CRyColorRampWidget widget;
    widget.show();

    return a.exec();
}
